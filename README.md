# <img src="resources/omnidoc-logo.png" alt="OmniDoc Logo" width="120px"/> Frontend developer assessment 

![OmniDoc Logo](resources/logo.jpeg)

## What are we looking for?

We are looking for members who are interested in being part of an agile, progressive, proactive team with new ideas.
We seek to use technology as an ally to reachr our goals as well as to offer customers a better user experience as well as delivering added value in our products.

## Assessment:

Using the next API Doc https://superheroapi.com/ create an app with the follow features:

- Search a character by name, show their powerstats, image and biography
- Add an action to save their info into a Database, (dont forget handle duplicated rows)
- Show the characters list saved from Database


NOTES:
- Feel free to design the UI for this app

### Requirement

- Add file README.md with instructions to run the project
- Backend Language (NodeJS or PHP)
- Frontend (Angular8+)

### Evaluation

- We´re goint to evaluate best practices, standards, logic, any effort to write clean code, maintainable, readeable etc.

Enjoy Coding!
